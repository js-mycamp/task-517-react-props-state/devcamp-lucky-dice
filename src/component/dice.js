import { Component } from "react";
import imageX from "../assets/image/LuckyDiceImages/people-playing-casino.jpg"
import imageDefault from "../assets/image/LuckyDiceImages/dice.png"
import image1 from "../assets/image/LuckyDiceImages/1.png"
import image2 from "../assets/image/LuckyDiceImages/2.png"
import image3 from "../assets/image/LuckyDiceImages/3.png"
import image4 from "../assets/image/LuckyDiceImages/4.png"
import image5 from "../assets/image/LuckyDiceImages/5.png"
import image6 from "../assets/image/LuckyDiceImages/6.png"

class Dice extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            imageShow: imageDefault,
            dice: 0
        })
    }

    onBtnNem = () => {
        let ranDom = Math.floor(Math.random() * 6) + 1;
        
        this.setState({
            dice: ranDom
        })
        if (this.state.dice === 1) {
            this.setState({
                imageShow: image1
            })
        }
        if (this.state.dice === 2) {
            this.setState({
                imageShow: image2
            })
        }
        if (this.state.dice === 3) {
            this.setState({
                imageShow: image3
            })
        }
        if (this.state.dice === 4) {
            this.setState({
                imageShow: image4
            })
        }
        if (this.state.dice === 5) {
            this.setState({
                imageShow: image5
            })
        }
        if (this.state.dice === 6) {
            this.setState({
                imageShow: image6
            })
        }

    }
    render() {
        return (
            <>
                <div>
                    <img src={imageX} alt="nguoiChoi" />;
                </div>

                <div className="mt-4">
                    <img src={this.state.imageShow} alt="dice" style={{height:"200px", width:"200px",textAlign:'center'}} />;
                </div>

                <div className="mt-4">
                    <button className="btn btn-primary" onClick={this.onBtnNem }>Ném</button>
                </div>
            </>
        )
    }
}
export default Dice