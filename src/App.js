import "bootstrap/dist/css/bootstrap.min.css";
import Dice from "./component/dice";
function App() {
  return (
  <>
   <div className="jumbotron text-center mt-4">
    <div className="row justify-content-center mt-4">
      <Dice />
    </div>
   </div>
  </>
  );
}

export default App;
